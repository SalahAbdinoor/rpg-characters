# RPG Characters

## Assignment

Use plain Java to create a console application with the following minimum requirements:

    a. Various character classes having attributes which increase at different rates as the character gains levels.
    b) Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the power of
       the character, causing it to deal more damage and be able to survive longer. Certain characters can equip certain item types.
    c) Custom exceptions. There are two custom exceptions you are required to write.
    d) Full test coverage of the functionality. Some testing data is provided.

## Usage

### run [Play](src/main/java/Play.java) for a quick demo

### Init new Hero

If you want to initialize a new hero you'll get hero-type that will
start with level 1 and base stats specified for that class. 

hero damage comes from different attributes depending on class (mage = intellect, ranger = dexterity)

Your hero can level up, equip weapons/armor & damage.

### Equipping Item

Heroes are only allowed to equip Items of same/lower level.

## Contributers

SA (@salahabdinor)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change. Please make sure to update tests as appropriate.