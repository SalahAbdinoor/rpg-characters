import attribute.PrimaryAttributes;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import hero.classes.Mage;
import enums.ArmorType;
import enums.Slots;
import enums.WeaponType;
import hero.classes.Ranger;
import hero.classes.Rogue;
import hero.classes.Warrior;
import item.types.Armor;
import item.types.Weapon;

public class Play {

    public static void main(String[] args) {

        // ------------ Mage ---------------
        Mage mage = new Mage("Cleopatra");

        PrimaryAttributes mageHeadStats = new PrimaryAttributes(2, 1, 4);
        PrimaryAttributes mageBodyStats = new PrimaryAttributes(4, 8, 3);
        PrimaryAttributes mageLegsStats = new PrimaryAttributes(4, 7, 7);
        PrimaryAttributes mageNewHeadStats = new PrimaryAttributes(10, 1, 24);

        Armor mageHead = new Armor("Helm of Hades", 1, Slots.HEAD, mageHeadStats, ArmorType.CLOTH);
        Armor mageBody = new Armor("Sand-worn Relic", 2, Slots.BODY, mageBodyStats, ArmorType.CLOTH);
        Armor mageLegs = new Armor("Vessel of Profound Possibilities", 4, Slots.LEGS, mageLegsStats, ArmorType.CLOTH);
        Armor newMageHead = new Armor("Spatial Opener", 1, Slots.HEAD, mageNewHeadStats, ArmorType.CLOTH);

        Weapon mageStaff = new Weapon("Greedy Driftwood Pole", 3, 8, 3, WeaponType.STAFFS);

        /* Role play */


        try {

            System.out.println("\n---------------- Level 1: 0 equipment\n" + mage + "\n----------------\n");

            mage.levelUp();
            mage.setArmor(Slots.HEAD, mageHead);
            System.out.println("\n---------------- Level 2: 1 equipment\n" + mage + "\n----------------\n");

            mage.levelUp();
            mage.setArmor(Slots.BODY, mageBody);
            System.out.println("\n---------------- Level 3: 2 equipment\n" + mage + "\n----------------\n");

            mage.levelUp();
            mage.setArmor(Slots.LEGS, mageLegs);
            System.out.println("\n---------------- Level 4: 3 equipment\n" + mage + "\n----------------\n");

            mage.levelUp();
            mage.setWeapon(mageStaff);
            System.out.println("\n---------------- Level 5: 4 equipment\n" + mage + "\n----------------\n");

            mage.setArmor(Slots.HEAD, newMageHead);
            System.out.println("\n---------------- Level 5: 4 equipment (new head)\n" + mage + "\n----------------\n");


        } catch (InvalidWeaponException | InvalidArmorException e) {
            throw new RuntimeException(e);
        }

        // ------------ !Mage ---------------


        // ------------ Warrior ---------------

        Warrior warrior = new Warrior("Sven");

        warrior.levelUp();
        warrior.levelUp();

        PrimaryAttributes warriorBodyStats = new PrimaryAttributes(24, 12, 3);

        Weapon warriorAxe = new Weapon("Oak Splitter", 3, 12, 6, WeaponType.AXES);
        Armor warriorPlateChest = new Armor("Glandular Armor", 2, Slots.BODY, warriorBodyStats, ArmorType.PLATE);

        try {

            warrior.setWeapon(warriorAxe);
            warrior.setArmor(Slots.BODY, warriorPlateChest);

        } catch (InvalidWeaponException | InvalidArmorException e) {
            e.printStackTrace();
        }

        //System.out.println(warrior);

        // ------------ !Warrior ---------------

        // ------------ Ranger ---------------

        Ranger ranger = new Ranger("Amr-ingot");

        PrimaryAttributes rangerLegsStats = new PrimaryAttributes(14, 18, 6);

        Weapon rangerBow = new Weapon("Dwarves Black Bow of Fate", 2, 9, 10, WeaponType.BOWS);
        Armor rangersLegs = new Armor("Writ's Legs", 1, Slots.LEGS, rangerLegsStats, ArmorType.LEATHER);

        ranger.levelUp();

        try {

            ranger.setWeapon(rangerBow);
            ranger.setArmor(Slots.LEGS, rangersLegs);

        } catch (InvalidWeaponException | InvalidArmorException e) {
            throw new RuntimeException(e);
        }


        //System.out.println(ranger);

        // ------------ !Ranger ---------------


        // ------------ Rogue ---------------

        Rogue rogue = new Rogue("Forsythia D'Neve");


        PrimaryAttributes rogueBodyStats = new PrimaryAttributes(17, 12, 7);

        Weapon roguesDagger = new Weapon("Umbraco Blades", 1, 9, 10, WeaponType.DAGGERS);
        Armor roguesBody = new Armor("Bogies Vest", 1, Slots.BODY, rogueBodyStats, ArmorType.MAIL);

        rogue.levelUp();


        try {

            rogue.setWeapon(roguesDagger);
            rogue.setArmor(Slots.BODY, roguesBody);

        } catch (InvalidWeaponException | InvalidArmorException e) {
            throw new RuntimeException(e);
        }

        //System.out.println(rogue);

        // ------------ !Rogue ---------------
    }
}
