package hero;

import attribute.PrimaryAttributes;
import enums.Slots;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import item.types.Armor;
import item.Item;
import item.types.Weapon;

import java.util.HashMap;

/**
 * This abstract class serves as...
 */
public abstract class Hero {

    /* Global variables */
    private final String name;
    private final PrimaryAttributes basePrimaryAttributes;
    private final HashMap<Slots, Item> itemSlot;
    private int level;
    private PrimaryAttributes totalPrimaryAttributes;

    /* Constructor */

    /**
     * Init new Hero
     * <p>
     * -- With slots for items (weapon/armor)
     *
     * @param name           name of hero
     * @param initAttributes base stats of hero-type
     */
    public Hero(String name, PrimaryAttributes initAttributes) {
        this.name = name;
        this.level = 1;
        this.basePrimaryAttributes = initAttributes;
        this.totalPrimaryAttributes = initAttributes;
        itemSlot = new HashMap<>();
    }

    public abstract boolean setWeapon(Weapon weapon) throws InvalidWeaponException;

    public abstract boolean setArmor(Slots slot, Armor armor) throws InvalidArmorException;

    public abstract void levelUp();

    public abstract double damage();

    /* Set Armor & Weapon */
    protected boolean setWeaponInSlot(Weapon weapon) throws InvalidWeaponException {

        // If weapon level is higher than hero level
        if (weapon.getLevel() > getLevel()) {
            // InvalidWeaponException
            throw new InvalidWeaponException("\nWeapon level is too high, hero needs: \n- " + (weapon.getLevel() - getLevel()) + " more level/levels");

        } else {
            // Weapon gets placed in slot
            this.itemSlot.put(Slots.WEAPONS, weapon);

            // updates totalPrimaryAttributes
            updateTotalPrimaryAttributes();
            return true;
        }
    }

    public boolean setArmorInSlot(Slots slot, Armor armor) throws InvalidArmorException {

        // If armor level is higher than hero level
        if (armor.getLevel() > getLevel()) {
            // InvalidArmorException
            throw new InvalidArmorException("\nArmor level is too high, hero needs: \n- " + (armor.getLevel() - getLevel()) + " more level/levels");
        } else {
            // armor gets placed in slot
            this.itemSlot.put(slot, armor);

            // updates totalPrimaryAttributes
            updateTotalPrimaryAttributes();
            return true;
        }
    }

    /* other functions */
    public double heroDPS(double totalMainPrimaryAttribute) {

        double heroDps;
        double weaponDps;

        // if no weapon is equipped
        if (getWeapon() != null)
            weaponDps = getWeapon().getDPS();
        else
            weaponDps = 1; // set dps to 1

        // Character DPS = Weapon DPS * (1 + TotalMainPrimaryAttribute/100)
        heroDps = weaponDps * (1 + totalMainPrimaryAttribute / 100);

        return heroDps;
    }

    /**
     * updates totalPrimaryAttributes
     */
    public void updateTotalPrimaryAttributes() {

        int strength = getBasePrimaryAttributes().getStrength();
        int dexterity = getBasePrimaryAttributes().getDexterity();
        int intelligence = getBasePrimaryAttributes().getIntelligence();

        // attributes of each item
        PrimaryAttributes attributes;

        // for each equipped item
        for (HashMap.Entry<Slots, Item> entry : getItemSlot().entrySet()) {

            Slots slot = entry.getKey();

            if (slot.equals(Slots.WEAPONS)) {
                // weapons do not add to totalPrimaryAttribute
                continue;
            } else {

                // get attribute from armor
                attributes = getArmorBySlot(slot).getPrimaryAttribute();

                // adding each armor attribute
                strength += attributes.getStrength();
                dexterity += attributes.getDexterity();
                intelligence += attributes.getIntelligence();
            }
        }

        // attributes from all equipped armor + base primary attributes
        // TODO: Set total instead of new.
        this.totalPrimaryAttributes = new PrimaryAttributes(strength, dexterity, intelligence);
    }

    /* Getters, Setters & Misc */
    public Weapon getWeapon() {
        return (Weapon) this.itemSlot.get(Slots.WEAPONS);
    }

    public Armor getArmorBySlot(Slots slot) {
        return (Armor) this.itemSlot.get(slot);
    }

    public HashMap<Slots, Item> getItemSlot() {
        return itemSlot;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public PrimaryAttributes getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    public PrimaryAttributes getTotalPrimaryAttributes() {
        return totalPrimaryAttributes;
    }

    public String toString() {

        return
                "\n\t- name: '" + name +
                        '\'' + ", \n\t- level: " + level +
                        ", \n\t- base attributes: " + basePrimaryAttributes +
                        ", \n\t- equipment{ " + getItemToString() + "\n\t}" +
                        ", \n\t- total attributes: " + totalPrimaryAttributes +
                        ", \n\t- total damage: " + damage();
    }

    public String getItemToString() {

        StringBuilder str = new StringBuilder();

        // collects name of each item
        for (HashMap.Entry<Slots, Item> entry : getItemSlot().entrySet()) {

            // If armor item string
            if (entry.getKey().equals(Slots.WEAPONS))
                str.append("\n\t\tItem { slot: ").append(entry.getKey())
                        .append(", name: ").append(entry.getValue().getName())
                        .append(", level: ").append(entry.getValue().getLevel())
                        .append(", damage: ").append(((Weapon) entry.getValue()).getDamage())
                        .append(", attackSpeed: ").append(((Weapon) entry.getValue()).getAttackSpeed())
                        .append(", type: ").append(((Weapon) entry.getValue()).getWeaponType())
                        .append(" },");
                // If armor item string
            else
                str.append("\n\t\tItem { slot: ").append(entry.getKey())
                        .append(", name: ").append(entry.getValue().getName())
                        .append(", level: ").append(entry.getValue().getLevel())
                        .append(", type: ").append(((Armor) entry.getValue()).getArmorType())
                        .append(", attributes: ").append(((Armor) entry.getValue()).getPrimaryAttribute())
                        .append(" },");
        }
        return str.toString();
    }
}