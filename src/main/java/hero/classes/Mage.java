package hero.classes;

import attribute.PrimaryAttributes;
import hero.Hero;
import enums.ArmorType;
import enums.Slots;
import enums.WeaponType;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import item.types.Armor;
import item.Item;
import item.types.Weapon;

public class Mage extends Hero {

    /**
     * init mage level 1
     * <p>
     * Base attributes:
     * -- Strength: 1
     * -- Dexterity: 1
     * -- Intelligence: 8
     *
     * @param name name of new mage
     */
    public Mage(String name) {
        super(name, new PrimaryAttributes(1, 1, 8));
    }

    @Override
    public boolean setWeapon(Weapon weapon) throws InvalidWeaponException {

        // IF weapon is made for mage
        if (weapon.getWeaponType().equals(WeaponType.STAFFS) || weapon.getWeaponType().equals(WeaponType.WANDS))
            // Set weapon in hero
            return setWeaponInSlot(weapon);
        else // IF weapon is NOT  made for mage
            // throw InvalidWeaponException
            throw new InvalidWeaponException("\nWeapon-type is not allowed for MAGE: " + weapon.getWeaponType()
                    + "\n ALLOWED Weapons: STAFFS & WANDS"
            );
    }

    @Override
    public boolean setArmor(Slots slot, Armor armor) throws InvalidArmorException {

        // IF armor does NOT 'suit' mage
        if (!armor.getArmorType().equals(ArmorType.CLOTH)) {
            //InvalidArmorException
            throw new InvalidArmorException("\nArmor-type is not allowed for MAGE: " + armor.getArmorType()
                    + "\n ALLOWED Armor: CLOTH");
        } else {
            // Set armor (in correct slot) in hero
            return setArmorInSlot(slot, armor);
        }
    }

    /**
     * level up mage
     * -- increase level by 1
     * -- new "Primary attributes"
     * -- update totalPrimaryAttributes
     */
    @Override
    public void levelUp() {

        // increase level
        setLevel(getLevel() + 1);

        // old attributes
        PrimaryAttributes attributes = getBasePrimaryAttributes();

        // increase new attribute (class specific)
        attributes.setStrength(attributes.getStrength() + 1);
        attributes.setDexterity(attributes.getDexterity() + 1);
        attributes.setIntelligence(attributes.getIntelligence() + 5);

        // update totalPrimaryAttributes
        updateTotalPrimaryAttributes();
    }

    /**
     * damage output based on mage
     * - mages damage derives from intellect
     *
     * @return damage per second
     */
    @Override
    public double damage() {
        // used to calculate damage
        double totalMainPrimaryAttribute = getTotalPrimaryAttributes().getIntelligence();

        return heroDPS(totalMainPrimaryAttribute);
    }

    @Override
    public String toString() {
        return "Mage:" +
                super.toString();
    }
}