package hero.classes;

import attribute.PrimaryAttributes;
import enums.ArmorType;
import enums.Slots;
import enums.WeaponType;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import hero.Hero;
import item.types.Armor;
import item.types.Weapon;

public class Ranger extends Hero {

    /**
     * init RANGER level 1
     * <p>
     * Base attributes:
     * -- Strength: 1
     * -- Dexterity: 7
     * -- Intelligence: 1
     *
     * @param name name of new mage
     */
    public Ranger(String name) {
        super(name, new PrimaryAttributes(1, 7, 1));
    }

    @Override
    public boolean setWeapon(Weapon weapon) throws InvalidWeaponException {

        // IF weapon is made for ranger
        if (weapon.getWeaponType().equals(WeaponType.BOWS))
            // Set weapon in hero
            return setWeaponInSlot(weapon);
        else // IF weapon is NOT  made for ranger
            // throw InvalidWeaponException
            throw new InvalidWeaponException("\nWeapon-type is not allowed for RANGER: " + weapon.getWeaponType()
                    + "\n ALLOWED Weapons: BOWS"
            );
    }


    @Override
    public boolean setArmor(Slots slot, Armor armor) throws InvalidArmorException {

        // IF armor does NOT 'suit' ranger
        if (!armor.getArmorType().equals(ArmorType.MAIL) && !armor.getArmorType().equals(ArmorType.LEATHER)) {
            //InvalidArmorException
            throw new InvalidArmorException("\nArmor-type is not allowed for RANGER: " + armor.getArmorType()
                    + "\n ALLOWED Armor: MAIL & LEATHER");
        } else {
            // Set armor (in correct slot) in hero
            return setArmorInSlot(slot, armor);
        }
    }

    /**
     * level up ranger
     * -- increase level by 1
     * -- new "Primary attributes"
     * -- update totalPrimaryAttributes
     */
    @Override
    public void levelUp() {

        // increase level
        setLevel(getLevel() + 1);

        // old attributes
        PrimaryAttributes attributes = getBasePrimaryAttributes();

        // increase new attribute (class specific)
        attributes.setStrength(attributes.getStrength() + 1);
        attributes.setDexterity(attributes.getDexterity() + 5);
        attributes.setIntelligence(attributes.getIntelligence() + 1);

        // update totalPrimaryAttributes
        updateTotalPrimaryAttributes();
    }

    /**
     * damage output based on ranger
     * - ranger damage derives from dexterity
     *
     * @return damage per second
     */
    @Override
    public double damage() {
        // used to calculate damage
        double rangerTotalDexterity = getTotalPrimaryAttributes().getDexterity();

        return heroDPS(rangerTotalDexterity);
    }

    @Override
    public String toString() {
        return "Ranger:" +
                super.toString();
    }
}
