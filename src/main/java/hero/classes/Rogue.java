package hero.classes;

import attribute.PrimaryAttributes;
import enums.ArmorType;
import enums.Slots;
import enums.WeaponType;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import hero.Hero;
import item.types.Armor;
import item.Item;
import item.types.Weapon;

public class Rogue extends Hero {
    /**
     * init rogue level 1
     * <p>
     * Base attributes:
     * -- Strength: 2
     * -- Dexterity: 6
     * -- Intelligence: 1
     *
     * @param name name of new rogue
     */
    public Rogue(String name) {
        super(name, new PrimaryAttributes(2, 6, 1));
    }

    @Override
    public boolean setWeapon(Weapon weapon) throws InvalidWeaponException {

        // IF weapon is made for rogue
        if (weapon.getWeaponType().equals(WeaponType.DAGGERS) || weapon.getWeaponType().equals(WeaponType.SWORDS))
            // Set weapon in hero
            return setWeaponInSlot(weapon);
        else // IF weapon is NOT  made for rogue
            // throw InvalidWeaponException
            throw new InvalidWeaponException("\nWeapon-type is not allowed for ROGUE: " + weapon.getWeaponType()
                    + "\n ALLOWED Weapons: DAGGERS & SWORDS"
            );
    }


    @Override
    public boolean setArmor(Slots slot, Armor armor) throws InvalidArmorException {

        // IF armor does NOT 'suit' rogue
        if (!armor.getArmorType().equals(ArmorType.LEATHER) && !armor.getArmorType().equals(ArmorType.MAIL)) {
            //InvalidArmorException
            throw new InvalidArmorException("\nArmor-type is not allowed for ROGUE: " + armor.getArmorType()
                    + "\n ALLOWED Armor: LEATHER & MAIL");
        } else {
            // Set armor (in correct slot) in hero
            return setArmorInSlot(slot, armor);
        }
    }

    /**
     * level up rogue
     * -- increase level by 1
     * -- new "Primary attributes"
     * -- update totalPrimaryAttributes
     */
    @Override
    public void levelUp() {

        // increase level
        setLevel(getLevel() + 1);

        // old attributes
        PrimaryAttributes attributes = getBasePrimaryAttributes();

        // increase new attribute (class specific)
        attributes.setStrength(attributes.getStrength() + 1);
        attributes.setDexterity(attributes.getDexterity() + 4);
        attributes.setIntelligence(attributes.getIntelligence() + 1);

        // update totalPrimaryAttributes
        updateTotalPrimaryAttributes();
    }

    /**
     * damage output based on rogue
     * - rogues damage derives from dexterity
     *
     * @return damage per second
     */
    @Override
    public double damage() {
        // used to calculate damage
        double rogueTotalDexterity = getTotalPrimaryAttributes().getDexterity();

        return heroDPS(rogueTotalDexterity);
    }

    @Override
    public String toString() {
        return "Rogue:" +
                super.toString();
    }
}
