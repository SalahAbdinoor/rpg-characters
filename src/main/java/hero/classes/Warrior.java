package hero.classes;

import attribute.PrimaryAttributes;
import enums.ArmorType;
import enums.Slots;
import enums.WeaponType;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import hero.Hero;
import item.types.Armor;
import item.Item;
import item.types.Weapon;

public class Warrior extends Hero {

    /**
     * init warrior level 1
     * <p>
     * Base attributes:
     * -- Strength: 5
     * -- Dexterity: 2
     * -- Intelligence: 1
     *
     * @param name name of new warrior
     */
    public Warrior(String name) {
        super(name, new PrimaryAttributes(5, 2, 1));
    }

    @Override
    public boolean setWeapon(Weapon weapon) throws InvalidWeaponException {

        // IF weapon is made for warrior
        if (weapon.getWeaponType().equals(WeaponType.AXES) || weapon.getWeaponType().equals(WeaponType.HAMMERS) || weapon.getWeaponType().equals(WeaponType.SWORDS))
            // Set weapon in hero
            return setWeaponInSlot(weapon);
        else // IF weapon is NOT  made for warrior
            // throw InvalidWeaponException
            throw new InvalidWeaponException("\n Weapon-type is not allowed for WARRIOR: " + weapon.getWeaponType()
                    + "\n ALLOWED Weapons: AXES, HAMMERS & SWORDS"
            );
    }

    @Override
    public boolean setArmor(Slots slot, Armor armor) throws InvalidArmorException {
        // IF armor does NOT 'suit' warrior
        if (!armor.getArmorType().equals(ArmorType.MAIL) && !armor.getArmorType().equals(ArmorType.PLATE)) {
            //InvalidArmorException
            throw new InvalidArmorException("\nArmor-type is not allowed for WARRIOR: " + armor.getArmorType()
                    + "\n ALLOWED Armor: MAIL & PLATE");
        } else {
            // Set armor (in correct slot) in hero
            return setArmorInSlot(slot, armor);
        }
    }


    /**
     * level up warrior
     * -- increase level by 1
     * -- new "Primary attributes"
     * -- update totalPrimaryAttributes
     */
    @Override
    public void levelUp() {

        // increase level
        setLevel(getLevel() + 1);

        // old attributes
        PrimaryAttributes attributes = getBasePrimaryAttributes();

        // increase new attribute (class specific)
        attributes.setStrength(attributes.getStrength() + 3);
        attributes.setDexterity(attributes.getDexterity() + 2);
        attributes.setIntelligence(attributes.getIntelligence() + 1);

        // update totalPrimaryAttributes
        updateTotalPrimaryAttributes();
    }

    /**
     * damage output based on warrior
     * - warriors damage derives from strength
     *
     * @return damage per second
     */
    @Override
    public double damage() {
        // used to calculate damage
        double warriorTotalStrength = getTotalPrimaryAttributes().getStrength();

        return heroDPS(warriorTotalStrength);
    }

    @Override
    public String toString() {
        return "Warrior:" +
                super.toString();
    }
}
