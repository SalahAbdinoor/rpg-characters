package item;

import enums.Slots;

public abstract class Item {
    private final String name;
    private final int level;
    private final Slots slot;

    public Item(String name, int level, Slots slot) {
        this.name = name;
        this.level = level;
        this.slot = slot;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return "Item{ " +
                "name='" + name + '\'' +
                ", level=" + level +
                ", slot=" + slot +
                " }";
    }
}
