package item.types;

import attribute.PrimaryAttributes;
import enums.ArmorType;
import enums.Slots;
import item.Item;

public class Armor extends Item {

    private final PrimaryAttributes primaryAttributes;

    private final ArmorType armorType;

    public Armor(String name, int level, Slots slot, PrimaryAttributes primaryAttributes, ArmorType armorType) {
        super(name, level, slot);
        this.primaryAttributes = primaryAttributes;
        this.armorType = armorType;
    }

    public PrimaryAttributes getPrimaryAttribute() {
        return primaryAttributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    @Override
    public String toString() {
        return
                super.toString() +
                        ", " + primaryAttributes +
                        ", armorType = " + armorType;
    }
}
