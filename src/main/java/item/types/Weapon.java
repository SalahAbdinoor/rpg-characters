package item.types;

import enums.Slots;
import enums.WeaponType;
import item.Item;

public class Weapon extends Item {
    private final double damage;
    private final double attackSpeed;
    private final WeaponType weaponType;

    public Weapon(String name, int level, double damage, double attackSpeed, WeaponType weaponType) {
        super(name, level, Slots.WEAPONS);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.weaponType = weaponType;
    }

    public double getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    /**
     * DPS = Damage * Attack speed
     *
     * @return equipped weapons dps
     */
    public double getDPS() {
        return this.damage * this.attackSpeed;
    }

    @Override
    public String toString() {
        return
                super.toString() +
                        ", damage=" + damage +
                        ", attackSpeed=" + attackSpeed +
                        ", weaponType=" + weaponType;
    }
}
