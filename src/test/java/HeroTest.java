import hero.classes.Mage;
import hero.classes.Ranger;
import hero.classes.Rogue;
import hero.classes.Warrior;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class HeroTest {

    private Mage mage;
    private Ranger ranger;
    private Rogue rogue;
    private Warrior warrior;

    @BeforeEach
    void initHero() {
        mage = new Mage("mage_name");
        ranger = new Ranger("ranger_name");
        rogue = new Rogue("rogue_name");
        warrior = new Warrior("warrior_name");
    }

    @Test
    void Init_MageIsLevelOneWhenCreated_ReturnsOne() {
        int actualLevel = mage.getLevel();
        int expectedLevel = 1;

        Assertions.assertEquals(actualLevel, expectedLevel);

        mage.levelUp();
    }

    @Test
    void LevelUp_HeroIsLevelTwoAfterLevelUp_ReturnsTwo() {

        // Expected
        int expectedLevel = 2;

        // invoking: Level Up
        mage.levelUp();

        // Actual
        int actualLevel = mage.getLevel();

        // Assertion
        Assertions.assertEquals(actualLevel, expectedLevel);
    }

    @Test
    void GetBaseAttributes_MageLevelOne_Success() {

        // Expected
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;

        // Actual
        int actualStrength = mage.getBasePrimaryAttributes().getStrength();
        int actualDexterity = mage.getBasePrimaryAttributes().getDexterity();
        int actualIntelligence = mage.getBasePrimaryAttributes().getIntelligence();

        // Assertion
        Assertions.assertEquals(actualStrength, expectedStrength);
        Assertions.assertEquals(actualDexterity, expectedDexterity);
        Assertions.assertEquals(actualIntelligence, expectedIntelligence);

    }

    @Test
    void GetBaseAttributes_RangerLevelOne_Success() {

        // Expected
        int expectedStrength = 1;
        int expectedDexterity = 7;
        int expectedIntelligence = 1;

        // Actual
        int actualStrength = ranger.getBasePrimaryAttributes().getStrength();
        int actualDexterity = ranger.getBasePrimaryAttributes().getDexterity();
        int actualIntelligence = ranger.getBasePrimaryAttributes().getIntelligence();

        // Assertion
        Assertions.assertEquals(actualStrength, expectedStrength);
        Assertions.assertEquals(actualDexterity, expectedDexterity);
        Assertions.assertEquals(actualIntelligence, expectedIntelligence);
    }

    @Test
    void GetBaseAttributes_RogueLevelOne_Success() {

        // Expected
        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedIntelligence = 1;

        // Actual
        int actualStrength = rogue.getBasePrimaryAttributes().getStrength();
        int actualDexterity = rogue.getBasePrimaryAttributes().getDexterity();
        int actualIntelligence = rogue.getBasePrimaryAttributes().getIntelligence();

        // Assertion
        Assertions.assertEquals(actualStrength, expectedStrength);
        Assertions.assertEquals(actualDexterity, expectedDexterity);
        Assertions.assertEquals(actualIntelligence, expectedIntelligence);

    }

    @Test
    void GetBaseAttributes_WarriorLevelOne_Success() {

        // Expected
        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;

        // Actual
        int actualStrength = warrior.getBasePrimaryAttributes().getStrength();
        int actualDexterity = warrior.getBasePrimaryAttributes().getDexterity();
        int actualIntelligence = warrior.getBasePrimaryAttributes().getIntelligence();

        // Assertion
        Assertions.assertEquals(actualStrength, expectedStrength);
        Assertions.assertEquals(actualDexterity, expectedDexterity);
        Assertions.assertEquals(actualIntelligence, expectedIntelligence);

    }

    @Test
    void GetBaseAttributes_MageLevelTwo_Success() {

        // Expected: baseLevel + gained level
        int expectedStrength = 1 + 1;
        int expectedDexterity = 1 + 1;
        int expectedIntelligence = 8 + 5;

        // Invoke: Level Up
        mage.levelUp();

        // Actual
        int actualStrength = mage.getBasePrimaryAttributes().getStrength();
        int actualDexterity = mage.getBasePrimaryAttributes().getDexterity();
        int actualIntelligence = mage.getBasePrimaryAttributes().getIntelligence();

        // Assertion
        Assertions.assertEquals(actualStrength, expectedStrength);
        Assertions.assertEquals(actualDexterity, expectedDexterity);
        Assertions.assertEquals(actualIntelligence, expectedIntelligence);

    }

    @Test
    void GetBaseAttributes_RangerLevelTwo_Success() {

        // Expected: baseLevel + gained level
        int expectedStrength = 1 + 1;
        int expectedDexterity = 7 + 5;
        int expectedIntelligence = 1 + 1;

        // Invoke: Level Up
        ranger.levelUp();

        // Actual
        int actualStrength = ranger.getBasePrimaryAttributes().getStrength();
        int actualDexterity = ranger.getBasePrimaryAttributes().getDexterity();
        int actualIntelligence = ranger.getBasePrimaryAttributes().getIntelligence();

        // Assertion
        Assertions.assertEquals(actualStrength, expectedStrength);
        Assertions.assertEquals(actualDexterity, expectedDexterity);
        Assertions.assertEquals(actualIntelligence, expectedIntelligence);

    }

    @Test
    void GetBaseAttributes_RogueLevelTwo_Success() {

        // Expected: baseLevel + gained level
        int expectedStrength = 2 + 1;
        int expectedDexterity = 6 + 4;
        int expectedIntelligence = 1 + 1;

        // Invoke: Level Up
        rogue.levelUp();

        // Actual
        int actualStrength = rogue.getBasePrimaryAttributes().getStrength();
        int actualDexterity = rogue.getBasePrimaryAttributes().getDexterity();
        int actualIntelligence = rogue.getBasePrimaryAttributes().getIntelligence();

        // Assertion
        Assertions.assertEquals(actualStrength, expectedStrength);
        Assertions.assertEquals(actualDexterity, expectedDexterity);
        Assertions.assertEquals(actualIntelligence, expectedIntelligence);

    }

    @Test
    void GetBaseAttributes_WarriorLevelTwo_Success() {

        // Expected: baseLevel + gained level
        int expectedStrength = 5 + 3;
        int expectedDexterity = 2 + 2;
        int expectedIntelligence = 1 + 1;

        // Invoke: Level Up
        warrior.levelUp();

        // Actual
        int actualStrength = warrior.getBasePrimaryAttributes().getStrength();
        int actualDexterity = warrior.getBasePrimaryAttributes().getDexterity();
        int actualIntelligence = warrior.getBasePrimaryAttributes().getIntelligence();

        // Assertion
        Assertions.assertEquals(actualStrength, expectedStrength);
        Assertions.assertEquals(actualDexterity, expectedDexterity);
        Assertions.assertEquals(actualIntelligence, expectedIntelligence);
    }
}
