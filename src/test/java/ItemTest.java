import attribute.PrimaryAttributes;
import enums.ArmorType;
import enums.Slots;
import enums.WeaponType;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;
import hero.classes.Warrior;
import item.types.Armor;
import item.types.Weapon;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ItemTest {

    @Test
    void SetWeapon_WarriorEquipsHighLevelWeapon_ExceptionThrown() {

        // warrior level 1
        Warrior testWarrior = new Warrior("warrior_name");
        // weapon level 2
        Weapon testWarriorAxe = new Weapon("Oak Splitter", 2, 12, 6, WeaponType.AXES);

        // actual exception
        InvalidWeaponException exception = Assertions.assertThrows(
                InvalidWeaponException.class, () -> {

                    // invoke: setWeapon()
                    testWarrior.setWeapon(testWarriorAxe);
                }
        );
        // assertion
        Assertions.assertEquals(exception.getMessage(),
                "\nWeapon level is too high, hero needs: " +
                        "\n- 1 more level/levels");
    }

    @Test
    void SetArmor_WarriorEquipsHighLevelArmor_ExceptionThrown() {

        // warrior level 1
        Warrior testWarrior = new Warrior("warrior_name");
        // armor level 2
        Armor testWarriorBodyPlate = new Armor("Gamboled Cuisine", 2,
                Slots.BODY, new PrimaryAttributes(24, 12, 3), ArmorType.PLATE);

        // actual exception
        InvalidArmorException exception = Assertions.assertThrows(
                InvalidArmorException.class, () -> {

                    // invoke: setArmor()
                    testWarrior.setArmor(Slots.HEAD, testWarriorBodyPlate);
                }
        );
        // assertion
        Assertions.assertEquals(exception.getMessage(),
                "\nArmor level is too high, hero needs: \n- 1 more level/levels");
    }

    @Test
    void SetWeapon_WarriorEquipsInvalidWeapon_ExceptionThrown() {

        // warrior level 1
        Warrior testWarrior = new Warrior("warrior_name");
        // weapon: bow
        Weapon testWarriorBow = new Weapon("Dwarves Black Bow of Fate",
                2, 9, 10, WeaponType.BOWS);

        // actual exception
        InvalidWeaponException exception = Assertions.assertThrows(
                InvalidWeaponException.class, () -> {

                    // invoke: setWeapon()
                    testWarrior.setWeapon(testWarriorBow);
                }
        );
        // assertion
        Assertions.assertEquals(exception.getMessage(),
                "\n Weapon-type is not allowed for WARRIOR: BOWS" +
                        "\n ALLOWED Weapons: AXES, HAMMERS & SWORDS");
    }

    @Test
    void SetWeapon_WarriorEquipsInvalidArmor_ExceptionThrown() {

        // warrior
        Warrior testWarrior = new Warrior("warrior_name");
        // weapon: bow
        Armor testWarriorHead = new Armor("Helm of Hades", 1, Slots.HEAD,
                new PrimaryAttributes(2, 1, 4), ArmorType.CLOTH);

        // actual exception
        InvalidArmorException exception = Assertions.assertThrows(
                InvalidArmorException.class, () -> {

                    // invoke: setArmor()
                    testWarrior.setArmor(Slots.HEAD, testWarriorHead);
                }
        );
        // assertion
        Assertions.assertEquals(exception.getMessage(),
                "\nArmor-type is not allowed for WARRIOR: CLOTH" +
                        "\n ALLOWED Armor: MAIL & PLATE");
    }

    @Test
    void SetWeapon_WarriorEquipsValidWeapon_ReturnsTrue() {

        // warrior
        Warrior testWarrior = new Warrior("warrior_name");

        // weapon: axe
        Weapon warriorAxe = new Weapon("Oak Splitter", 1, 12, 6, WeaponType.AXES);

        // expected boolean
        boolean weaponIsEquippedExpected = true;

        // actual boolean (TRUE)
        boolean weaponIsEquippedActual;
        try {
            // invoke: setWeapon()
            weaponIsEquippedActual = testWarrior.setWeapon(warriorAxe);

        } catch (InvalidWeaponException e) {
            throw new RuntimeException(e);
        }

        // assertion
        Assertions.assertEquals(weaponIsEquippedExpected, weaponIsEquippedActual);
    }

    @Test
    void SetArmor_WarriorEquipsValidArmor_ReturnsTrue() {

        // warrior
        Warrior testWarrior = new Warrior("warrior_name");

        // armor: body plate
        Armor testWarriorBodyPlate = new Armor("Gamboled Cuisine", 1,
                Slots.BODY, new PrimaryAttributes(24, 12, 3), ArmorType.PLATE);

        // actual boolean
        boolean armorIsEquipped;

        try {
            // invoke: setArmor()
            armorIsEquipped = testWarrior.setArmor(Slots.BODY, testWarriorBodyPlate);

        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }

        // assertion
        Assertions.assertTrue(armorIsEquipped);
    }

    @Test
    void getTotalAttributes_UnEquippedWarrior_equalsBase() {

        // warrior
        Warrior testWarrior = new Warrior("warrior_name");

        // expected total PrimaryAttribute
        PrimaryAttributes expectedTotalAttributes = testWarrior.getTotalPrimaryAttributes();

        // actual total PrimaryAttribute
        PrimaryAttributes actualTotalAttributes = new PrimaryAttributes(5, 2, 1);

        // assertion
        Assertions.assertEquals(expectedTotalAttributes.toString(), actualTotalAttributes.toString());
    }

    @Test
    void getTotalAttributes_ArmorEquippedWarrior_equalsBase() {

        // warrior
        Warrior testWarrior = new Warrior("warrior_name");

        // armor: body plate
        Armor testWarriorBodyPlate = new Armor("Gamboled Cuisine", 1,
                Slots.BODY, new PrimaryAttributes(1, 2, 3), ArmorType.PLATE);

        try {
            // invoke: setArmor()
            testWarrior.setArmor(Slots.BODY, testWarriorBodyPlate);
        } catch (InvalidArmorException e) {
            throw new RuntimeException(e);
        }

        // expected total PrimaryAttribute
        PrimaryAttributes expectedTotalAttributes =
                new PrimaryAttributes(5 + 1, 2 + 2, 1 + 3);

        // actual total PrimaryAttribute
        PrimaryAttributes actualTotalAttributes = testWarrior.getTotalPrimaryAttributes();

        // assertion
        Assertions.assertEquals(expectedTotalAttributes.toString(), actualTotalAttributes.toString());
    }

    @Test
    void getDPS_UnEquippedWarriorLevelOne_ReturnsDPS() {

        // warrior
        Warrior testWarrior = new Warrior("warrior_name");

        // expected double
        double expectedDPS = 1.05;

        // actual double
        double actualDPS = testWarrior.damage();

        // assertion
        Assertions.assertEquals(expectedDPS, actualDPS);
    }

    @Test
    void getDPS_WeaponEquippedWarriorLevelOne_ReturnsDPS() {

        // warrior
        Warrior testWarrior = new Warrior("warrior_name");

        // weapon: axe
        Weapon testWarriorAxe = new Weapon("Oak Splitter", 1, 1.1, 7, WeaponType.AXES);

        try {
            // invoke: setWeapon()
            testWarrior.setWeapon(testWarriorAxe);
        } catch (InvalidWeaponException e) {
            throw new RuntimeException(e);
        }

        // expected double
        double expectedDPS = 8.085;

        // actual double
        double actualDPS = testWarrior.damage();

        // assertion
        Assertions.assertEquals(expectedDPS, actualDPS);
    }

    @Test
    void getDPS_WeaponAndArmorEquippedWarriorLevelOne_ReturnsDPS() {

        // warrior
        Warrior testWarrior = new Warrior("warrior_name");

        // weapon: axe
        Weapon testWarriorAxe = new Weapon("Oak Splitter", 1, 1.1, 7, WeaponType.AXES);
        // armor: body plate
        Armor testWarriorBodyPlate = new Armor("Gamboled Cuisine", 1,
                Slots.BODY, new PrimaryAttributes(1, 2, 3), ArmorType.PLATE);

        try {
            // invoke: setWeapon()
            testWarrior.setWeapon(testWarriorAxe);
            testWarrior.setArmor(Slots.BODY, testWarriorBodyPlate);
        } catch (InvalidWeaponException | InvalidArmorException e) {
            throw new RuntimeException(e);
        }

        // expected double
        double expectedDPS = 8.162;

        // actual double
        double actualDPS = testWarrior.damage();

        // assertion
        Assertions.assertEquals(expectedDPS, actualDPS);
    }


}


